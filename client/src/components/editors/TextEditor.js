import React, { useState } from 'react';

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

function TextEditor( {validateTextInput, formValues, setFormValues, formValid, setFormValid, article}) {

 

        console.log("FORMVALUES",formValues)
        const handleChange = (html) =>{
            
            validateTextInput('article', html,5, formValues,setFormValues, formValid, setFormValid)
          
        }
   
    return (<ReactQuill theme="snow" defaultValue={article}  onChange={handleChange}   />);
}

export default TextEditor;