import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faXmark} from '@fortawesome/free-solid-svg-icons';



// const items = [
//     <img src="https://placehold.co/200x200" onDragStart={handleDragStart} role="presentation" />,
//     <img src="https://placehold.co/200x200" onDragStart={handleDragStart} role="presentation" />,
//     <img src="https://placehold.co/200x200" onDragStart={handleDragStart} role="presentation" />,
//   ];


  const responsive = {
   0:{
    items: 1
   }
  }

function ImagesEditor({imagesPreview}){
    
    console.log("imgEdit",imagesPreview)
    


    return (
     
        <AliceCarousel mouseTracking
         items={imagesPreview} 
         responsive={ responsive}
         disableDotsControls={true} />
        
      );
   
}




export default ImagesEditor;