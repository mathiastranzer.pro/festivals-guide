import { useState , createContext} from 'react';
import {useBlog} from '../../hooks/blogHooks/useBlog'
const BlogContext = createContext();

function BlogProvider({children}){

      
    const [formValid, setFormValid] = useState({title :  `L'entrée doit faire plus de 30 caractères`, 
                                                article : `L'entrée doit faire plus de 500 caractères` ,
                                                })
    
    const [formValues, setFormValues] = useState({title : '', 
                                                   article : '' ,
                                                   images: []
                                                    });

    const [imageFiles, setImageFiles] = useState([]);

    const {articles, submitArticle, deleteArticle, updateArticle} = useBlog();
    const  [formOpen, setFormOpen] = useState(false);
    


    return(
        <BlogContext.Provider value={{ formValid, setFormValid, formValues, setFormValues,
        articles, submitArticle, deleteArticle, updateArticle,imageFiles,
         setImageFiles, formOpen, setFormOpen}}>
            {children}
        </BlogContext.Provider>
    )

}

export{
    BlogContext,
    BlogProvider
}