import { useState } from 'react';
import {useBlog} from '../../hooks/blogHooks/useBlog'
import Article from './Article';
import ArticleForm from './ArticleForm';

import '../../assets/css/UserList.css';
import '../../assets/css/User.css'

  function  ArticlesList(){
    
    const {articles, submitArticle, deleteArticle, updateArticle} = useBlog();
    const  [formOpen, setFormOpen] = useState(false);
    
    console.log(articles);

  


    let addIcon = formOpen ? '':'icons';
      
    return    (
         <div className='table'>
         <div className='tabHeader tabRow titleRow'> <div>Titre</div><div>Auteur</div><div>Image</div> <div>Date</div>
         <div>Editer</div><div>Supprimer</div>
         <div className={addIcon} onClick={()=>{formOpen == false ? setFormOpen(true):setFormOpen(false)}}>{formOpen ? <span> x &emsp;&emsp;&emsp;</span> : '+'}</div></div>
         <div>{formOpen ?  <ArticleForm submitArticle={submitArticle} /> : null}</div>
         <div className='scrolling'>
            {Array.isArray(articles) ? articles.map(({id, title, article, user, createdAt, images}) => { 
              return(
                <Article 
                key={id}
                id={id} 
                title={title}
                article={article}
                user={user}
                images ={images}
                createdAt={createdAt}
                deleteArticle={deleteArticle}
                updateArticle={updateArticle}
                />)
             }):<h1>Pas d'article à afficher</h1>}
             </div>
         </div>
    )
  }
  
export default ArticlesList;


