import { useState, useEffect } from 'react';

import TextEditor from '../editors/TextEditor';
import ImagesEditor from '../editors/imagesEditor';

import '../../assets/css/ArticleForm.css'
import { validateTextInput, isPropertiesAreTrue } from '../../services/validation';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faXmark, faFileArrowUp, faTrash} from '@fortawesome/free-solid-svg-icons';


function ArticleForm({ submitArticle, updateArticle, title, article, images}){

    console.log(images)


   
    const [formValid, setFormValid] = useState({title :  `L'entrée doit faire plus de 30 caractères`, 
                                                article : `L'entrée doit faire plus de 500 caractères` ,
                                                })
    
    const [formValues, setFormValues] = useState({title : '', 
                                                   article : '' ,
                                                   images: []
                                                    });

    const [imageFiles, setImageFiles] = useState([]);

    

   
    

    const handleDragStart = (e) => e.preventDefault();

    function removeImg(imgId, imgArray, elt){
        
        setImageFiles(imageFiles.filter(image => image.lastModified !== imgId))
        
    }

    console.log("IMG", imageFiles)
    // if(images.lenght > 1){
    //     setImageFiles(images);
    // }

const imagesDisplay = []
    
// if(imagesDisp){
//     imagesDisplay.concat(imagesDisp.map((image) =>{

//         return <div id={image.lastModified} className='imgContainer'><img src={image.path}  role="presentation" onDragStart={handleDragStart} width={200} height={200} /><div className='imgIcon' onClick={(e)=>removeImg(image.lastModified, imageFiles, e)}><FontAwesomeIcon icon={faTrash} /></div></div>
          
//         }))
//     }
   const imagesPreview = imageFiles.map((image) =>{
       
        var url = URL.createObjectURL(image);
        return <div id={image.lastModified} className='imgContainer'><img src={url}  role="presentation" onDragStart={handleDragStart} width={200} height={200} /><div className='imgIcon' onClick={(e)=>removeImg(image.lastModified, imageFiles, e)}><FontAwesomeIcon icon={faTrash} /></div></div>
      
    }).reverse();

   
    console.log('IMG A JOUR', imagesDisplay)
   
    return(
         <div className=' articleFormRow articleForm'>
            <div className='txtColumn'>
                <p>Titre : </p>
                <section className='articleTitle'>
                <input name="title" value={title}  required onChange={(e) =>  {validateTextInput('title', e.target.value,5, formValues,setFormValues, formValid, setFormValid)}}  />
                </section>
               <p>Article : </p>
               <section className='articleEditor'  >  
                    <TextEditor  validateTextInput={validateTextInput}
                                    formValues={formValues}
                                    setFormValues={setFormValues}
                                    formValid={formValid}
                                    setFormValid={setFormValid}
                                    article={article}
                                />
                </section>    
            </div>
            <div className="imageColumn"> 
            <section className="imageInput">
            <span>Choisir une image : <label for="file" className="label-file"> <FontAwesomeIcon className='upload' icon={faFileArrowUp} /></label></span>
              <input className='input-file' id="file" name='image' type='file' multiple required onChange={(e)=>{setFormValues({...formValues, images : [...formValues.images,{ path: `${e.target.files[0].name}`, imageType: 'article' }]});setImageFiles([...imageFiles, e.target.files[0]]) }}/>
              </section>
          
            <section className="imgEdit"><ImagesEditor imagesPreview = {imagesPreview } /></section>
            
            </div>
            <div  className="articleCmdColumn" ><div><FontAwesomeIcon className=" icons" icon={faCheck}  onClick={()=>{ isPropertiesAreTrue(formValid) ? submitArticle(formValues, imageFiles) : console.log('invalide') }} /></div></div>
        </div>
    )
}


//<textarea name="article"  required onChange={(e) =>  {validateTextInput('article', e.target.value,5, formValues,setFormValues, formValid, setFormValid)}} />
export default ArticleForm;

