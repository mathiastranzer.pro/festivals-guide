import { useState } from 'react';
import { dateToFrFormat } from '../../services/date';

import ImagesEditor from '../editors/imagesEditor';
import ArticleForm from './ArticleForm';

import '../../assets/css/User.css'
import'../../assets/css/Articles.css'


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faInfo,faXmark, faLockOpen, faLock} from '@fortawesome/free-solid-svg-icons'


function Article({id, title, user, article, images, createdAt,  deleteArticle, updateArticle, submitArticle}){
  
    const  [isOpen, setIsOpen] = useState(false);
    console.log('ARTICLE',article)
    
    //console.log('adminStatus',adminStatus.id);

    const date = dateToFrFormat(createdAt);

    function updateArticle(id, adminStatus){
    
   // updateUserAdmin({id:id, isAdmin: adminStatus[id]})
   }
    const imagesDisp = [].concat(images);

    const handleDragStart = (e) => e.preventDefault();

    //const imageData =  <img src={images[0].path}  role="presentation" onDragStart={handleDragStart} width={100} height={100} />
    const imagesGallery  = imagesDisp.map((image) =>{

    return <img src={image.path}  role="presentation" onDragStart={handleDragStart} width={80} height={80} />
      
    })
     
    return(
     <div className='userRow'>
     <div className='tabRowArt'><div>{title}</div><div>{user === null ? 'inconnu' : user.firstName } {user === null ? '' : user.lastName}</div><div> <section className='caroussel'><ImagesEditor imagesPreview = {imagesGallery } /></section></div> <div>{date}</div><div className='icons' onClick={()=>{isOpen == true ?setIsOpen(false):setIsOpen(true)}}>{isOpen == false ?<FontAwesomeIcon icon={faInfo} /> : <span>x</span>}</div>
     <div className="icons" onClick={()=>{ deleteArticle(id)}} ><FontAwesomeIcon icon={faTrash} /></div><div className='emptyColumn'></div>
    
        </div>
        {isOpen ?  < ArticleForm
                          submitArticle={submitArticle}
                          updateArticle={updateArticle}
                          title={title}
                          article={article}
                          images = {images}
                                             /> : null
        }
        </div>
     )
     
}

export default Article;
/*<div className=' tabRow pullDownSection titleRow userForm'>
            <div><div >Date de création</div><section className='infoData'>{date}</section></div>
            <div><div>Image</div><section className='infoData'>{images.length >= 1 ? images[0].path : 'non renseigné'}</section></div>
            <div><div>Type</div><section className='infoData'>{images.length >= 1 ? images[0].imageType : 'non renseigné'}</section></div>
            <div ><section className='infoData'><FontAwesomeIcon className="icons" icon={faXmark}  onClick={()=>{setIsClose(true)}} /></section></div>
            </div>*/