import {BrowserRouter as Router, Routes, Route}from 'react-router-dom'
import NavBar from './navBar/NavBar';
import UserList from './users/UserList'
import ArticlesList from './blog/ArticlesList'
import Error from './Error/Error';
import {BlogContext} from './contexts/BlogContext'

function App() {
  return (
    <Router>
      <div className ='main'>
      <NavBar/>
      <Routes>
     
      <Route path="/blogadmin" element={<ArticlesList />} />
     
       <Route path="/usersadmin" element={<UserList/>} />
       <Route path="*" element={<Error/>} />
      
      </Routes>
    </div>
   </Router>
  );
}

export default App;
