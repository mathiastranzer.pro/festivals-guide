import {Link} from 'react-router-dom';
import '../../assets/css/NavBar.css'

function NavBar(){

    return( 
    <nav className='navBar_style'>
    <Link to="/usersadmin">Gestion des utilisateurs</Link>
    <Link to="/blogadmin">Gestion du blog</Link>
    </nav>)
}

export default NavBar;