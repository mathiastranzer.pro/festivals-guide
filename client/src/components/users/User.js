import { useState } from 'react';
import { dateToFrFormat } from '../../services/date';

import '../../assets/css/User.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faInfo,faXmark, faLockOpen, faLock} from '@fortawesome/free-solid-svg-icons'


function User({id, firstName, lastName, email, isAdmin, addresses, createdAt, adminStatus, setAdminStatus, deleteUser, updateUserAdmin}){
  
    const  [isClose, setIsClose] = useState(true);
    
    const date = dateToFrFormat(createdAt);

    function updateAdmin(id, adminStatus){
        console.log('adminTest',  adminStatus[id] )
        updateUserAdmin({id:id, isAdmin: adminStatus[id]})
    }
    
     
    return(
     <div className='userRow'>
     <div className='tabRow'> <div>{firstName}</div> <div>{lastName}</div> <div>{email}</div><div className='icons' onClick={()=>setIsClose(false)}><FontAwesomeIcon icon={faInfo} /></div>
     <div className="icons" onClick={()=>{ deleteUser(id)}} ><FontAwesomeIcon icon={faTrash} /></div>
     <div>{isAdmin ? <FontAwesomeIcon icon={faLockOpen} /> : ' '}</div></div>
    {isClose ? null: <div className=' tabRow pullDownSection titleRow userForm'>
            <div><div >Date de création</div><section className='infoData'>{date}</section></div>
            <div><div>Adresse</div><section className='infoData'>{addresses.length >= 1 ? addresses[0].address : 'non renseigné'}</section></div>
            <div><div>Ville</div><section className='infoData'>{addresses.length >= 1 ? addresses[0].city : 'non renseigné'}</section></div>
            <div><div>Code postal</div><section className='infoData'>{addresses.length >= 1 ? addresses[0].postalCode.toString() : 'non renseigné'}</section></div>
            <div className="icons" > <section className='infoData' onClick={()=>{adminStatus[id] ? setAdminStatus({...adminStatus, [id]: false}) : setAdminStatus({...adminStatus, [id] : true}); updateAdmin(id, adminStatus)}}>
            {isAdmin ? <FontAwesomeIcon icon={faLockOpen} /> : <FontAwesomeIcon icon={faLock} />}</section></div>
            <div ><section className='infoData'><FontAwesomeIcon className="icons" icon={faXmark}  onClick={()=>{setIsClose(true)}} /></section></div>
            </div>
        }
        </div>
     )
}

export default User;


