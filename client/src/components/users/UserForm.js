import { useState, useEffect,} from 'react';
import { validateTextInput, validateEmailInput, isPropertiesAreTrue } from '../../services/validation';

import '../../assets/css/UserForm.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck, faXmark, faLock, faLockOpen} from '@fortawesome/free-solid-svg-icons';


function UserForm({formOpen,setFormOpen, useUsers, submitUser}){

    
    const [formValid, setFormValid] = useState({firstName :  `L'entrée doit faire plus de 4 caractères`, 
                                                lastName : `L'entrée doit faire plus de 4 caractères` ,
                                                email   : `L'entrée doit être un email valide`,
                                                password : `L'entrée doit faire plus de 4 caractères`,
                                                })
    
    const [formValues, setFormValues] = useState({firstName : '', 
                                                   lastName : '' ,
                                                    email   : '',
                                                    password : '',
                                                    isAdmin : false});
    
 console.log(formValues)
// const isValid = {...formValid}
  
    return(
         <div className='tabRow titleRow userForm form'>
         <div><input name="firstName"  required onChange={(e) =>  {validateTextInput('firstName', e.target.value,4, formValues,setFormValues, formValid, setFormValid)}}  /><div className="validMessage" >{formValid.firstName}</div></div>
            <div><input name="lastName"  required onChange={(e) =>  {validateTextInput('lastName', e.target.value,4, formValues,setFormValues, formValid, setFormValid)}} /><div className="validMessage" >{formValid.lastName}</div></div>
            <div><input type='email' name="email"  required onChange={(e) =>  {validateEmailInput('email', e.target.value, formValues, setFormValues, formValid, setFormValid)}} /><div className="validMessage" >{formValid.email}</div></div>
            <div><input type='password' name="password"  required onChange={(e) =>  {validateTextInput('password', e.target.value,4, formValues,setFormValues, formValid, setFormValid)}} /><div className="validMessage" >{formValid.password}</div></div>
            <div className=" icons" onClick={()=>{ formValues.isAdmin ? setFormValues({...formValues, isAdmin: false}) : setFormValues({...formValues, isAdmin: true})}}>{formValues.isAdmin ? <FontAwesomeIcon icon={faLockOpen} /> : <FontAwesomeIcon icon={faLock} />}</div>
            <div ><span>&ensp;</span><FontAwesomeIcon className=" icons" icon={faCheck}  onClick={()=>{ isPropertiesAreTrue(formValid) ? submitUser(formValues) : console.log('invalide') }} /><span>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</span><FontAwesomeIcon className=" icons" icon={faXmark}  onClick={()=>{setFormOpen(false)}} /></div></div>
    )
}


export default UserForm;
