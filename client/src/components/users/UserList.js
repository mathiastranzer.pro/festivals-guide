import { useState } from 'react';
import {useUsers} from '../../hooks/userHooks/useUsers'
import User from './User';
import UserForm from './UserForm';

import '../../assets/css/UserList.css';
import '../../assets/css/User.css'

  function  UserList(){
    
    const {users, submitUser, deleteUser, updateUserAdmin} = useUsers();
    const  [formOpen, setFormOpen] = useState(false);
    const [adminStatus, setAdminStatus] = useState({});

    let addIcon = formOpen ? '':'icons';
      
    return(
         <div className='table'>
         <div className='tabHeader tabRow titleRow'> <div>Prénom</div> <div>Nom</div> <div>@</div>
         <div>{formOpen ? 'Mot de passe' : 'Info'}</div><div>{formOpen ? 'Admin' : 'Supprimer'}</div>
         <div className={addIcon} onClick={()=>setFormOpen(true)}>{formOpen ? <span>Valider &emsp;&emsp;&emsp; Fermer</span> : '+'}</div></div>
         <div>{formOpen ?  <UserForm formOpen={formOpen} setFormOpen={setFormOpen} submitUser={submitUser}  /> : null}</div>
         <div className='scrolling'>
            {users?.map(({id, firstName, lastName, email, isAdmin, addresses, createdAt}) => { 
              return(
                <User 
                key={id}
                id={id} 
                firstName={firstName}
                lastName={lastName}
                email={email}
                isAdmin={isAdmin}
                addresses ={addresses}
                createdAt={createdAt}
                setAdminStatus={setAdminStatus}
                adminStatus={adminStatus}
                deleteUser={deleteUser}
                updateUserAdmin={updateUserAdmin}
                />)
             })}
             </div>
         </div>
    )
  }
  
export default UserList;


