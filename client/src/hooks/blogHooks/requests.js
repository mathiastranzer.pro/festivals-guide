
async function httpGetArticles(){
    const URL = `http://localhost:8000/api/articles`;
    
    try {
        const query = await fetch(URL)
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpPostArticle(article){
    const URL = `http://localhost:8000/api/articles`;

    const config = {
        method: "POST",
        body: article
    }
    
    try {
       
        const query = await fetch(URL,config)
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpDeleteArticle(id){
    const URL = `http://localhost:8000/api/articles/${id}`;
    
    try {
        const query = await fetch(URL,
            {
                method : "delete",
            });

        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpUpdateArticle(article){
    const URL = `http://localhost:8000/api/users/admin`;
    
    try {
       
        const query = await fetch(URL,{
            method: "put",
            headers: {"Content-Type": "application/json"},
            body: article
          })
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

export {
    httpGetArticles,
    httpPostArticle,
    httpDeleteArticle,
    httpUpdateArticle,
    
}