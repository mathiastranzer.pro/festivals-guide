import { useCallback, useEffect, useState } from "react";
import { httpGetArticles, httpPostArticle,httpUpdateArticle, httpDeleteArticle } from "./requests";

export function useBlog(){

    const [articles, setArticles] = useState([]);

    const getArticles = useCallback(async ()=>{
        const fetchArticles = await httpGetArticles();
        setArticles(fetchArticles);
    }, []);

    useEffect(()=>{
        getArticles()
    },[getArticles])

    const submitArticle = useCallback(async (article, images) => {

        const data = new FormData();
        data.append('title', article.title);
        data.append('article', article.article);
        data.append('images', JSON.stringify(article.images));
       
   images.forEach((file, i) => {
     data.append('images', file, file.name)
   });

    

    const response = await httpPostArticle(data);

   
        if(response.created){
    
            getArticles();
        }else{
            console.log('Erreur')
        }
    }, [getArticles]);


    const deleteArticle = useCallback(async (userId) => {

        const response = await httpDeleteArticle(userId);
    
        console.log(response);
    
            if(response.deleted){
        
                getArticles();
                
            }else{

                console.log('Erreur')
            }
        }, [getArticles]);
    

        const updateArticle = useCallback(async (article) => {

            const response = await httpUpdateArticle(article);
        
            console.log(response);
        
                if(response.updated){
            
                    getArticles();

                }else{
                    
                    console.log('Erreur')
                }
            }, [getArticles]);
        
        
    return { articles,
            submitArticle,
            deleteArticle,
            updateArticle}
}

