
async function httpGetUsers(){
    const URL = `http://localhost:8000/api/users`;
    
    try {
        const query = await fetch(URL)
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpPostUser(user){
    const URL = `http://localhost:8000/api/users`;
    
    try {
       
        const query = await fetch(URL,{
            method: "post",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(user)
          })
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpDeleteUser(id){
    const URL = `http://localhost:8000/api/users/${id}`;
    
    try {
        const query = await fetch(URL,
            {
                method : "delete",
            });

        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

async function httpUpdateUserAdmin(user){
    const URL = `http://localhost:8000/api/users/admin`;
    
    try {
       
        const query = await fetch(URL,{
            method: "put",
            headers: {"Content-Type": "application/json"},
            body: JSON.stringify(user)
          })
        if (!query.ok) {

                throw new Error(query.status)

        } else {

        return await query.json();

        }      
    } catch (error) {

         console.log(error)
         return {ok : false}
    }
}

export {
    httpGetUsers,
    httpPostUser,
    httpDeleteUser,
    httpUpdateUserAdmin
}

