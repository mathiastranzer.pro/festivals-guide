import { useCallback, useEffect, useState } from "react";
import { httpGetUsers, httpPostUser, httpDeleteUser, httpUpdateUserAdmin } from "./requests";

export function useUsers(){

    const [users, setUsers] = useState([]);

    const getUsers = useCallback(async ()=>{
        const fetchUsers = await httpGetUsers();
        setUsers(fetchUsers);
    }, []);

    useEffect(()=>{
        getUsers()
    },[getUsers])

    const submitUser = useCallback(async (user) => {

    const response = await httpPostUser(user);

    console.log(response);

        if(response.created){
    
            getUsers();
        }else{
            console.log('Erreur')
        }
    }, [getUsers]);


    const deleteUser = useCallback(async (userId) => {

        const response = await httpDeleteUser(userId);
    
        console.log(response);
    
            if(response.deleted){
        
                getUsers();
            }else{
                console.log('Erreur')
            }
        }, [getUsers]);
    

        const updateUserAdmin = useCallback(async (user) => {

            const response = await httpUpdateUserAdmin(user);
        
            console.log(response);
        
                if(response.updated){
            
                    getUsers();
                }else{
                    console.log('Erreur')
                }
            }, [getUsers]);
        
   

    return { users,
            submitUser,
            deleteUser,
            updateUserAdmin}

}

