import validator from 'validator';



function validateTextInput(inputName, inputValue, stringLength, dataState, dataStateSetter, validationSate, validationStateSetter){
 
    if(inputValue.length >  stringLength){
        
        dataStateSetter({...dataState, [inputName]: inputValue});
        validationStateSetter({...validationSate, [inputName]: true});

        return true;
    
    }else{

        return  validationSate[inputName];
        
    }
}

function validateEmailInput(inputName, inputValue, dataState, dataStateSetter, validationSate, validationStateSetter){

    if( validator.isEmail(inputValue)){
       
        dataStateSetter({...dataState, [inputName]: inputValue});
        validationStateSetter({...validationSate, [inputName]: true});

        return true;
    
    }else{

        return validationSate[inputName];    
    
    }
}

function isPropertiesAreTrue(obj) {
    const isValid = Object.values(obj).every((value) => value === true);
    console.log('isValide ?', isValid);
    return isValid;
  }
  

export{
    validateTextInput,
    validateEmailInput,
    isPropertiesAreTrue
}