
function dateToFrFormat(dateString){

    let date = new Date(dateString);
    let options = { day: 'numeric', month: 'numeric', year: 'numeric' };

    return  date.toLocaleDateString('fr-FR', options);
}



export{
    dateToFrFormat
}