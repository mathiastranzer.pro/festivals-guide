const {
    getAddressesByUserId,
    newAddress,
    deleteAddressById,
    updateAddress
} = require('../../models/address.model');

const {getUserById} = require('../../models/user.model');

//Récupération des adresses par ID d'utilisateur
async function httpGetAdressesByUserId(req, res){
        
    const userId = req.params.id
    const addresses = await getAddressesByUserId(userId);
    console.log(addresses);

    if(addresses.error){

        return res.status(500).json({error : "Server error, please, try later "});
    };

    if (addresses.response == null) {

        return res.status(400).json({error: 'Not found'});
        
    } else {

        return res.status(201).json(addresses.response);
    }
}


// Création d'un nouvelle adresse.
async function httpCreateAdressByUserId(req, res){
    
    const addressData = req.body;

    if(! addressData.address || ! addressData.postalCode || ! addressData.city || ! addressData.userId ){

        return res.status(400).json({error : "Validation error "});
    }

    const isUserExist = await getUserById(addressData.userId);

    if(isUserExist.response == null){
        
        return res.status(400).json({error : "No user with this id"});

    }else if(isUserExist.error){

        return res.status(500).json({error : "Server error, please, try later "});

    }else{

        const result = await newAddress(addressData);

        if(result.error){

            return res.status(500).json({error : "Server error, please, try later "});

        }else{

            return res.status(201).json(result);
       
        }
    }
}


//Supprime une adresse par Id
async function httpDeleteAddressById(req, res){
    
    const addressId = req.params.id
    const deleted = await deleteAddressById(addressId);

    if(deleted.error){

        return res.status(500).json({error : "Server error, please, try later "});

    }else if(deleted === 0){

        return res.status(400).json({message: `Address not deleted`});

    }else{

        return res.status(201).json({message: `Address deleted`});

    }      
}

//Modifie une addresse
async function httpUpdateAddress(req, res){
    
    const addressData = req.body;

    if(! addressData.address || ! addressData.postalCode || ! addressData.city || ! addressData.userId ){

        return res.status(400).json({error : "Validation error "});
    }

    const updated = await updateAddress(addressData);

    if (updated === null || updated.error ) {

        return res.status(500).json({message: `update failed`});

    }else{

        return res.status(201).json(updated);

    }    
}


module.exports ={
    httpGetAdressesByUserId,
    httpCreateAdressByUserId,
    httpDeleteAddressById,
    httpUpdateAddress
}
