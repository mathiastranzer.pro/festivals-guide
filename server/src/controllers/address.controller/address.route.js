const express = require('express');

const { httpCreateAdressByUserId,
        httpGetAdressesByUserId,
        httpUpdateAddress,
        httpDeleteAddressById,
        } = require('./address.controller');

const router = express.Router();

router.post('/create',httpCreateAdressByUserId);
router.get('/:id', httpGetAdressesByUserId);
router.put('/',httpUpdateAddress);
router.delete('/:id', httpDeleteAddressById);


module.exports = router;