//const bcrypt = require('bcryptjs');
const { newUser, 
        getAllUsers, 
        getUserById, 
        deleteUserById, 
        updateUser } = require('../../models/user.model');

//Récupère les utilisateurs
async function httpGetAllUsers(req, res){
    
        const users = await getAllUsers();
   
        if(users.error){
           
            return res.status(500).json({error : 'Database Error'});
            
        }else{
            
            return res.status(200).json(users);
        }

}

//Récupération des utilisateurs par ID
async function httpGetUserById(req, res){
        
        const userId = req.params.id
        const user = await getUserById(userId);
        console.log(user);

        if(user.error){

            return res.status(500).json({error : "Server error, please, try later "});
        };

        if (user.response == null) {

            return res.status(400).json({error: 'Not found'});
            
        } else {

            return res.status(201).json(user.response);
        }
}


// Création d'un nouvel utilisateur. Todo enregistremenet des l'adresses
async function httpCreateUser(req, res){
        
    const userData = req.body;

    if(! userData.firstName || ! userData.lastName || ! userData.email || ! userData.password ){

        return res.status(400).json({error : "Validation error "});
    }
     
    const result = await newUser(userData);
    
    if(result.error){
        
        return res.status(500).json({error : "Server error, please, try later "});
        
    }else if(! result.created){
        
        return res.status(400).json({error : "This user already exist"});

    }else if(result){

        return res.status(201).json(result);

    }    
}


//Supprime un utilisateur par Id
async function httpDeleteUserById(req, res){
        
    const userId = req.params.id
    const deleted = await deleteUserById(userId);
    
    if(deleted.error){

        return res.status(500).json({error : "Server error, please, try later "});
    
    }else if(deleted === 0){

        return res.status(400).json({message: `User not deleted`});

    }else{

        return res.status(201).json({message: `User deleted`, deleted:true });

    }      
}

//Modifie un utilisateur
async function httpUpdateUser(req, res){
        
    const userData = req.body;

    if(! userData.firstName || ! userData.lastName || ! userData.email || ! userData.password ){

        return res.status(400).json({error : "Validation error "});
    }
    
    const updatedUser = await updateUser(userData);

    if (updatedUser === null || updatedUser.error ) {

        return res.status(500).json({message: `update failed`});
   
    }else{
        
        return res.status(201).json({updatedUser, updated : true});
   
    }    
}

async function httpUpdateUserAdmin(req, res){
        
    const userData = req.body;

    const updatedUser = await updateUser(userData);

    if (updatedUser === null || updatedUser.error ) {

        return res.status(500).json({message: `update failed`});
   
    }else{
        
        return res.status(201).json({updatedUser, updated : true});
   
    }    
}


//TODO
//Login Todi mis en place session côté client
async function httpLogin(req, res){
        const userName = req.body.name;
        
        res.status(200).json({message: `connecté ${userName}`})
}

//SignUp, Todo traitement identification
async function httpSignUp(req, res){
        const name = req.body.name;
        const password = req.body.password;

        res.status(200).json({nom : `${name}`,
                            password : `${password}`})      

}


module.exports = {
    httpGetAllUsers,
    httpGetUserById,
    httpCreateUser,
    httpDeleteUserById,
    httpUpdateUser,
    httpUpdateUserAdmin,
    httpLogin, 
    httpSignUp,
};