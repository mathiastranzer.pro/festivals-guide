const express = require('express');
const {httpLogin,
     httpSignUp,
     httpCreateUser, 
     httpGetAllUsers, 
     httpGetUserById, 
     httpDeleteUserById, 
     httpUpdateUser,
     httpUpdateUserAdmin} = require('./user.controller');

const router = express.Router();

router.post('/',httpCreateUser);
router.post('/login', httpLogin);
router.post('/signup', httpSignUp);
router.get('/', httpGetAllUsers );
router.get('/:id', httpGetUserById);
router.put('/',httpUpdateUser);
router.put('/admin', httpUpdateUserAdmin);
router.delete('/:id', httpDeleteUserById);

module.exports = router;