const { deleteImageById, 
        updateImage,
        newImage } = require('../../models/image.model');

const {getProductById} = require('../../models/product.model');


//ajouter une image

async function httpAddImage(req, res){
    
    const imageData = req.body;
    console.log(imageData);

 
    const product = await getProductById(imageData.productId);
    //const blogArticle = await getBlogArticleById(imageData.blogArticleId);
    
    if(product == null){
        return res.status(400).json({error : "this product or article id doesn't exist"});
    }
    
    const imagePath = 'images/';
    const result = await newImage(imageData, imagePath);
    
    if(result === false){
        
        return res.status(500).json({error : "Server error, please, try later "});  
    }

        return res.status(201).json(result);
        
}

//Supprimer une image

async function httpDeleteImage(req, res){
    const imageId = req.params.id
    const deleted = await deleteImageById(imageId);
   if(deleted === 0){
    return res.status(400).json({message: `Image not deleted`});
   }
    return res.status(201).json({message: `Image deleted`});
}

//Modifier une image

async function httpUpdateImage(req, res){
    const imageData = req.body;
    const updated = await updateImage(imageData);
    if (updated === null) {
        return res.status(500).json({message: `update failed`});
    }
    return res.status(201).json(updated);

}

module.exports = {
    httpDeleteImage,
    httpUpdateImage,
    httpAddImage
}