const express = require('express');

const {
    httpAddImage,
    httpDeleteImage,
    httpUpdateImage,
    } = require('./image.controller');

const router = express.Router();

router.post('/', httpAddImage);
router.put('/', httpUpdateImage);
router.delete('/:id',httpDeleteImage);



module.exports = router;

