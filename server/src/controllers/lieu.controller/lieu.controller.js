const path = require('path');
const { nvLieu,
    getAllLieu,
    getLieuById,
    updateLieu,
    deleteLieuById
} = require('../../models/lieu.model');

const {deleteFileByName} = require('../../services/files')


async function httpTestBlog(req, res){

        return res.status(200).json({message : 'Route ok'});
}

//Créer un article

async function httpNewLieu(req, res){

    const lieuData = req.body;
     console.log("DATA",req);

     const dataToSave = {
        nom : lieuData.nom,
        description : lieuData.description,
        latitude : lieuData.latitude,
        longitude : lieuData.longitude
        // images: JSON.parse(lieuData.images)
     }

    const result = await nvLieu(dataToSave);
    console.log("NV LIEU", result)
    
    if(result === false){
        
        return res.status(500).json({error : "Server error, please, try later "});  
    }

        return res.status(201).json(result);

}

//Récupérer tous les articles

async function httpGetAllLieux(req, res){
   
    const lieux = await getAllLieu();

    lieux.forEach(element => {
       
    const newImageUrl = element.dataValues.images.map(image=>{

            image.dataValues.path = `${req.protocol}://${req.get('host')}/images/${image.dataValues.path}`;

            return image;
        })

        element.dataValues.images = newImageUrl;
});
     
    if(lieux){

        return res.status(200).json(lieux);

    }else{

        console.log(`Can't get data`)
        return res.status(500).json({error : 'Database Error'});
    }
}

//Récupérer un article par id

async function httpGetLieuById(req, res){

    const lieuId = req.params.id
    const lieu = await getLieuById(lieuId);
    
    if (lieu != null) {

        return res.status(201).json(lieu);

    } else {

        return res.status(400).json({error: 'Not found'});

    }
}

//modifier un article

async function httpUpdateLieu(req, res){

    const lieuData = req.body;
    
    const updated = await updateLieu(lieuData);
    
    if (updated === null) {

        return res.status(500).json({message: `update failed`});
    }

    return res.status(201).json(updated);

}

//Supprimer un article

async function httpDeleteLieu(req, res){

    const lieuId = req.params.id
    const fileName = await getLieuById(lieuId);

    let isfileRemoved = false;
    let deleted = 0;
    
    fileName.dataValues.images.forEach(image =>{

        isFileRemoved = deleteFileByName(path.join(__dirname,'..','..','..','public','data','uploads', image.dataValues.path));
        console.log('isFileRemoved', isFileRemoved)

    })

    //if(isfileRemoved){

        deleted = await deleteLieuById(lieuId);
    
    //}

   if(deleted === 0){

    return res.status(400).json({message: `Article not deleted`});

   }

    return res.status(201).json({message: `Article deleted`, deleted:true});
}


module.exports = {
    httpTestBlog,
    httpNewLieu,
    httpGetAllLieux,
    httpGetLieuById,
    httpUpdateLieu,
    httpDeleteLieu
}