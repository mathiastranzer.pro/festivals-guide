const express = require('express');
const multer = require('../../services/multer')
const { 
    httpTestBlog,
    httpNewLieu,
    httpGetAllLieux,
    httpGetLieuById,
    httpUpdateLieu,
    httpDeleteLieu  
    } = require('./lieu.controller');

const router = express.Router();

router.get('/test', httpTestBlog);
router.post('/', httpNewLieu);
router.get('/', httpGetAllLieux);
router.get('/:id',httpGetLieuById);
router.put('/', httpUpdateLieu);
router.delete('/:id',httpDeleteLieu);



module.exports = router;