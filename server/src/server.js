require('dotenv').config();
const http = require('http');
const path = require('path');


const {createDb} = require('./services/sequelize');
const app = require('./app');

const server = http.createServer(app);

//createDb();

server.listen(8000);
server.on('listening', ()=>{
    console.log("Le serveur est disponible sur le port 8000");
    
})
server.on('error', (error)=>{
    console.log('Erreur', error);
})