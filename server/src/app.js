const path = require('path');
const express = require('express');
const cors = require('cors');

const userRouter = require('./controllers/user.controller/user.route');
const addressRouter = require('./controllers/address.controller/address.route');
//const productRouter = require('./controllers/product.controller/product.route');
const imageRouter = require('./controllers/image.controller/image.route');
const lieuRouter = require('./controllers/lieu.controller/lieu.route');
//const orderRouter = require('./controllers/order.controller/order.route');


const app = express();

 app.use(express.json());
//  app.use(express.urlencoded({limit: '5mb', extended: true}));
app.use(cors());

app.use('/images',express.static(path.join(__dirname,'..','public', 'data', 'uploads')));
console.log(path.join(__dirname,'..','public', 'data', 'uploads'))
app.use('/api/users', userRouter);
app.use('/api/addresses', addressRouter);
//app.use('/api/products', productRouter);
app.use('/api/images', imageRouter);
//app.use('/api/orders', orderRouter);
app.use('/api/lieux', lieuRouter);


module.exports = app;
