const { sequelize, DataTypes } = require('../services/sequelize');


const Image = sequelize.define('image',{
    
    path:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    imageType:{
        type: DataTypes.STRING,
        allowNull: false,
    }
})



async function updateImage(updatedImage){
    try {
        await Image.update({
            path: updatedImage.images.path,
            imageType: updatedImage.imageType
    
          }, {
            where: {
              id: updatedImage.id
            }
          });

          return await Image.findByPk(updatedImage.id);
        
    } catch (error) {
        console.log(error)
        return {status : false}    
    }
}

async function deleteImageById(id){
    try{
        const isDestroyed = await Image.destroy({
            where: {
            id: id
            },
        });
        return isDestroyed;
    }catch(error){
        console.log(error);
    }
}


async function newImage(newImage, imagePath){
    
    try{
        const image =  await Image.create({ 
            path : imagePath+newImage.path,
            imageType: newImage.imageType,
            blogArticleId : newImage.BlogArticleId,
            productId : newImage.productId

        });

        return {created : true, image}

    }catch(error){
        console.log(error);
         return false
    }
}


module.exports = {
    Image,
    updateImage,
    deleteImageById,
    newImage
}
