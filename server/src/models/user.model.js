const { sequelize, DataTypes } = require('../services/sequelize');

const { Address } = require('./address.model');

const User = sequelize.define('user',{
    firstName: {
        type: DataTypes.STRING,
        allowNull: false,

    },
    lastName:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    email:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    password:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    isAdmin:{
        type: DataTypes.BOOLEAN,
        allowNull: true,
    }
    
})

User.hasMany(Address,{ onDelete: 'cascade'});
Address.belongsTo(User);



async function getAllUsers(){
    
    try {

        return await User.findAll({ include: Address });

    }catch(err){
    
        console.log(err);

       return {error : true};

    }
}


async function getUserById(userId){

    try{

        return {response : await User.findByPk(userId, { include: Address })};
   
    }catch(err){
       
        return {error : true};
    
    }
}


async function newUser(userData){
    
    try{
        const [user, created] = await User.findOrCreate({
        where: { email: userData.email },
        defaults: {...userData}});

        return {created, user};

    }catch(err){
        console.log(err)
        return {error : true}
    }
}


async function deleteUserById(id){
    try{
        return  await User.destroy({
            where: {
            id: id
            },
        });
        
        
    }catch(err){
        console.log(err);
        return {error: true}
    }
}


async function updateUser(data){
   
    try{

    await User.update({ 
        firstName: data.firstName,
        lastName: data.lastName,
        email: data.email,
        password: data.password,
        isAdmin: data.isAdmin
        }, {
        where: {
          id: data.id
        }
      });
     
      return await User.findByPk(data.id);
      
    }catch(error){

        console.log(err);
        return {error : true}
    }
}

async function updateUserAdmin(data){
   
    try{

    await User.update({ 
        isAdmin: data.isAdmin
        }, {
        where: {
          id: data.id
        }
      });
     
      return await User.findByPk(data.id);
      
    }catch(error){

        console.log(err);
        return {error : true}
    }
}



module.exports = {
    User, 
    newUser,
    getAllUsers,
    getUserById,
    deleteUserById,
    updateUser,
    updateUserAdmin
}