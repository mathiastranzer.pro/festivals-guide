const { sequelize, DataTypes } = require('../services/sequelize');


const Address = sequelize.define('address',{
    
    address:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    postalCode:{
        type: DataTypes.INTEGER,
        allowNull: false,
    },
    city:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    
})



async function getAddressesByUserId(userId){

    try{

        return {response : await Address.findByPk(userId)};
   
    }catch(err){
       
        console.log(err);

        return {error : true};
    

    }
}


async function newAddress(addressData){
    
    try{

        const address = await Address.create({...addressData});

        return {created : true, address};
        
    }catch(err){

        console.log(err);

        return {error : true}

    }
}


async function deleteAddressById(id){
    try{
        return  await Address.destroy({
            where: {
            id: id
            },
        });  
        
    }catch(err){
        console.log(err);
        return {error: true}
    }
}


async function updateAddress(addressData){
   
    try{

       const addressUpdated = await Address.update(
            { ...addressData}, 
            {where: {id: addressData.id}
        });
        
        console.log(addressUpdated);
        return await Address.findByPk(addressData.id);
        
    }catch(error){

        console.log(err);
        return {error : true}
    }
}




module.exports = {
    Address,
    getAddressesByUserId,
    newAddress,
    deleteAddressById,
    updateAddress

}
