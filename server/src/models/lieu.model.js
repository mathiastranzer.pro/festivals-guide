const { sequelize, DataTypes } = require('../services/sequelize');
const {User} = require('./user.model');
const {Image} = require('./image.model');

const Lieu = sequelize.define('lieu',{
    nom: {
        type: DataTypes.STRING,
        allowNull: false,

    },
    description:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    latitude:{
        type: DataTypes.STRING,
        allowNull: false,
    },
    longitude:{
        type: DataTypes.STRING,
        allowNull: false,
    },
})



User.hasMany(Lieu);
Lieu.belongsTo(User);

Lieu.Image = Lieu.hasMany(Image); 
Image.belongsTo(Lieu);


async function nvLieu(nvLieu){
    
    try{
        
        const data = nvLieu.images;

        const article = await Lieu.create({ 
            nom : nvLieu.nom,
            description : nvLieu.description,
            latitude : nvLieu.latitude,
            longitude: nvLieu.longitude},
            // {include : [{ association: Lieu.Image}]}
            );
        
        return {created: true, article};

    }catch(error){
        console.log(error);
         return false
    }
}


async function getAllLieu(){
    
    try {
        return await Lieu.findAll({ include: {all: true} });
    }catch(error){
        console.log(error);
        return false;
    }
    
}


async function getLieuById(lieuId){
    try{

        return await Lieu.findByPk(lieuId, { include: Image })

    }catch(error){

        console.log(error);

    }
    
}

async function deleteLieuById(id){
    try{
        const isDestroyed = await Lieu.destroy({
            where: {
            id: id
            },
        });

        console.log("DESTROY", isDestroyed)
        
        return isDestroyed;

    }catch(error){

        console.log(error);
    }
}

async function updateLieu(updatedLieu){
    try{
    await Lieu.update({
        title : updatedLieu.title,
            article: updatedLieu.article,
        }, {
        where: {
          id: updatedLieu.id
        }
      });
    
      
      
      return await Lieu.findByPk(updatedLieu.id, { include: Image });
      
    }catch(error){

        console.log(error);
        return {status : false}

    }
}
// BlogArticle.sync();

console.log(Lieu === sequelize.models.User);

module.exports = {
    Lieu,
    nvLieu,
    getAllLieu,
    getLieuById,
    updateLieu,
    deleteLieuById
}
