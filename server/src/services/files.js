const fs = require('fs');

function deleteFileByName(fileName) {
  let resp = false;
  fs.unlink(fileName, (err) => {
    if (err) {
      console.error(`Error  ${fileName}: ${err}`);
      
    } else {
      console.log(`File ${fileName} has been removed.`);
      resp = true;
    }
  });

  return resp;
}

module.exports = {
    deleteFileByName
}