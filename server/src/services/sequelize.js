const { Sequelize, DataTypes } = require('sequelize');






const DBNAME =  process.env.DATABASENAME;
const DBUSER = process.env.DATABASEUSER;
const DBPW = process.env.DATABASEPW;
const DBHOST = process.env.DATABASEHOST;

console.log(DBPW);




const sequelize = new Sequelize(DBNAME, DBUSER, DBPW, {
    host: DBHOST,
    dialect: 'mariadb',
    logging: true,
  });

  async function testConnect(){
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }
  }

  async function createDb(){
    await sequelize.sync({ force: true });
}

  module.exports = {
    sequelize,
    DataTypes,
    testConnect,
    createDb
  }