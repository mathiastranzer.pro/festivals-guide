const multer = require('multer');

const MIME_TYPES ={
    'image/jpg' : 'jpg',
    'image/jpeg': 'png',
    'image/png': 'png'
}
const storage = multer.diskStorage({
    destination : (req, file, callback)=>{
        callback(null,  './public/data/uploads/');
    },
    filename: (req, file, callback)=>{
          
        const name = file.originalname.split(' ').join();
        const extension = MIME_TYPES[file.mimetype];
        const newName = name + Date.now() + '.' + extension;
        callback(null, newName)
       
        const newImages = JSON.parse(req.body.images).map(image => {
            if(image.path.includes(file.originalname)){
               
                image.path = newName;

            }
            return image ;
        });

        req.body.images = JSON.stringify(newImages);

    }
});


const upload = multer({ storage: storage })

module.exports = upload.array('images', 3);